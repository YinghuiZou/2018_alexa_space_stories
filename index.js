"use strict";

const Alexa = require("ask-sdk");
const cardTitle = "Space Stories";
const states = require("./utils/states");
//The things update in the playback nearly finished won't update to next
const cardContent = "";
const unhandledMessage =
  "I couldn't understand what you said, please say it again.";
const smallImageUrl =
  "https://s3.amazonaws.com/2018-alexa-space-stories/image/cards/Space+Stories+Cards_720x480_Page_1.png";
const largeImageUrl =
  "https://s3.amazonaws.com/2018-alexa-space-stories/image/cards/Space+Stories_Cards_1200x800_Page_1.png";
let template = {
  type: "BodyTemplate1",
  backgroundImage: {
    contentDescription: "Make a Mystery",
    sources: [{
      url: "https://s3.amazonaws.com/2018-alexa-space-stories/image/cards/Space+Stories_Cards_1200x800_Page_1.png"
    }]
  },
  backButton: "HIDDEN"
};
const categories = {
  history_of_space: 3,
  becoming_an_astronaut: 3,
  space_travel: 2,
  gravity_facts: 2,
  walking_in_space: 2,
  life_in_space: 6,
  cool_facts: 3
};

var StoryName = [
  "history_of_space",
  "history_of_space",
  "history_of_space",
  "becoming_an_astronaut",
  "becoming_an_astronaut",
  "becoming_an_astronaut",
  "space_travel",
  "space_travel",
  "gravity_facts",
  "gravity_facts",
  "walking_in_space",
  "walking_in_space",
  "life_in_space",
  "life_in_space",
  "life_in_space",
  "life_in_space",
  "life_in_space",
  "life_in_space",
  "cool_facts",
  "cool_facts",
  "cool_facts"
];
var StoryNumber = [
  1,
  2,
  3,
  1,
  2,
  3,
  1,
  2,
  1,
  2,
  1,
  2,
  1,
  2,
  3,
  4,
  5,
  6,
  1,
  2,
  3
];

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;

    return request.type === "LaunchRequest";
  },
  handle(handlerInput) {
    return new Promise((resolve, reject) => {
      handlerInput.attributesManager
        .getPersistentAttributes()
        .then(attributes => {
          handlerInput.attributesManager.setSessionAttributes(attributes);
          attributes.FirstIndex = 0;
          attributes.TEST = 1;
          attributes.offsetInMilliseconds = 0;
          const speech =
            '<audio src="https://s3.amazonaws.com/2018-alexa-space-stories/audio/intro_and_bio/spaceintro1.mp3"/>' +
            " Would you like to listen to Mike's biography? Say biography." +
            " Or say space story to choose a category for space stories.";
          return speech;
        })
        .then(speech => {
          if (
            handlerInput.requestEnvelope.context.System.device
            .supportedInterfaces.Display
          ) {
            resolve(
              handlerInput.responseBuilder
              .addRenderTemplateDirective(template)
              .speak(speech)
              .reprompt(speech)
              .getResponse()
            );
          } else {
            resolve(
              handlerInput.responseBuilder
              .speak(speech)
              .reprompt(speech)
              .withStandardCard(cardTitle, "", smallImageUrl, largeImageUrl)
              .getResponse()
            );
          }
        })
        .catch(error => {
          console.log("Launch Request Error: ", error);
          reject(error);
        });
    });
  }
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    const request = handlerInput.requestEnvelope.request;
    console.log(`Error handled: ${error.message}`);
    console.log(error.stack);

    if (
      handlerInput.requestEnvelope.context.System.device.supportedInterfaces
      .Display
    ) {
      return handlerInput.responseBuilder
        .speak(unhandledMessage)
        .reprompt(unhandledMessage)
        .addRenderTemplateDirective(template)
        .getResponse();
    } else {
      return handlerInput.responseBuilder
        .speak(unhandledMessage)
        .reprompt(unhandledMessage)
        .withStandardCard(cardTitle, "", smallImageUrl, largeImageUrl)
        .getResponse();
    }
  }
};

const AuthorBioIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "AuthorBioIntent"
    );
  },
  handle(handlerInput) {
    const speech =
      '<audio src ="https://s3.amazonaws.com/2018-alexa-space-stories/audio/intro_and_bio/biowithmusic1.mp3"/>' +
      ", say space story to choose a category from space stories, or say, learn more, to get more information about space.";

    if (
      handlerInput.requestEnvelope.context.System.device.supportedInterfaces
      .Display
    ) {
      return handlerInput.responseBuilder
        .speak(speech)
        .reprompt(speech)
        .addRenderTemplateDirective(template)
        .getResponse();
    } else {
      return handlerInput.responseBuilder
        .speak(speech)
        .reprompt(speech)
        .withStandardCard(cardTitle, "", smallImageUrl, largeImageUrl)
        .getResponse();
    }
  }
};
const SpaceStoryIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "spaceStoryIntent"
    );
  },
  handle(handlerInput) {
    const speechOutput =
      "you can say play all, to start from the beginning. Or, you can choose a category from the list on your skill card. If you would like to hear the list, say list.";
    const repromptSpeech = "choose a category.";

    if (
      handlerInput.requestEnvelope.context.System.device.supportedInterfaces
      .Display
    ) {
      return handlerInput.responseBuilder
        .speak(speechOutput)
        .reprompt(repromptSpeech)
        .addRenderTemplateDirective(template)
        .getResponse();
    } else {
      return handlerInput.responseBuilder
        .speak(speechOutput)
        .reprompt(repromptSpeech)
        .withStandardCard(cardTitle, "", smallImageUrl, largeImageUrl)
        .getResponse();
    }
  }
};

const ListStoryIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "listStoryIntent"
    );
  },
  handle(handlerInput) {
    const speechOutput =
      "The categories are. History of Space. Becoming an astronaut. Space Travel. Gravity facts. Walking in space. Life in space. Cool Facts. Please select a category or say play all to start from the beginning.";
    const repromptSpeech = "choose a category.";

    if (
      handlerInput.requestEnvelope.context.System.device.supportedInterfaces
      .Display
    ) {
      return handlerInput.responseBuilder
        .speak(speechOutput)
        .reprompt(repromptSpeech)
        .addRenderTemplateDirective(template)
        .getResponse();
    } else {
      return handlerInput.responseBuilder
        .speak(speechOutput)
        .reprompt(repromptSpeech)
        .withStandardCard(cardTitle, "", smallImageUrl, largeImageUrl)
        .getResponse();
    }
  }
};

/**
 *
 * @param {*} handlerInput
 * @param {*} attributes
 */
function play(handlerInput, attributes) {
  let category = attributes.category;
  let topic = attributes.topic;
  let offsetInMilliseconds = attributes.offsetInMilliseconds;
  let behavior = "REPLACE_ALL";
  let url = `https://s3.amazonaws.com/2018-alexa-space-stories/audio/${category}/${topic}.wav`;
  let token = url;
  let speechOutput = "You can say, Alexa, next, to listen to the next topic.";
  if (offsetInMilliseconds == 0) {
    if (
      handlerInput.requestEnvelope.context.System.device.supportedInterfaces
      .Display
    ) {
      return handlerInput.responseBuilder
        .addAudioPlayerPlayDirective(
          behavior,
          url,
          token,
          null,
          offsetInMilliseconds
        )
        .speak(speechOutput)
        .addRenderTemplateDirective(template)
        .getResponse();
    } else {
      return handlerInput.responseBuilder
        .addAudioPlayerPlayDirective(
          behavior,
          url,
          token,
          null,
          offsetInMilliseconds
        )
        .speak(speechOutput)
        .withStandardCard(cardTitle, "", smallImageUrl, largeImageUrl)
        .getResponse();
    }
  } else {

    console.log('It goes inside here for the offset is not equal to zero');
    console.log('The behavior for offeset is not equal zero is:' + behavior + 'url is: ' + url + 'token is: ' + token + 'Offset is: ' + offsetInMilliseconds);
    if (
      handlerInput.requestEnvelope.context.System.device.supportedInterfaces
      .Display
    ) {
      return handlerInput.responseBuilder
        .addAudioPlayerPlayDirective(
          behavior,
          url,
          token,
          offsetInMilliseconds,
          null
        )
        .addRenderTemplateDirective(template)
        .reprompt("")
        .getResponse();
    } else {
      return handlerInput.responseBuilder
        .addAudioPlayerPlayDirective(
          behavior,
          url,
          token,
          offsetInMilliseconds,
          null
        )
        .withStandardCard(cardTitle, "", smallImageUrl, largeImageUrl)
        .getResponse();
    }
  }
}

//This left for later

function playNearFinish(
  handlerInput,
  attributes,
  categorySize,
  nextNumber,
  offsetInMilliseconds,
  previousToken,
  CurrentToken,
  state
) {
  //So PlayNearly Functgion have both previous Token and Current Token.  Current token will track the previous's current token to see
  //if there is a match. If there is an match then it will return

  if (state === states.PLAYALL) {
    nextNumber = attributes.FirstIndex;
  }

  if (categorySize < nextNumber) {
    console.log(
      "Goes exit" + "size" + categorySize + "nextnumber" + nextNumber
    );
    return (
      handlerInput.responseBuilder
      //.addAudioPlayerStopDirective()
      .getResponse()
    );
  } else {
    if (
      handlerInput.requestEnvelope.context.System.device.supportedInterfaces
      .Display
    ) {
      let behavior = "ENQUEUE";
      console.log(
        "Goes inside the enqueue with " +
        "current token: " +
        CurrentToken +
        "preivous: " +
        previousToken +
        "behavior " +
        behavior,
        offsetInMilliseconds
      );
      return handlerInput.responseBuilder
        .addAudioPlayerPlayDirective(
          behavior,
          CurrentToken,
          CurrentToken,
          offsetInMilliseconds,
          previousToken
        )
        //  .addRenderTemplateDirective(template)
        .getResponse();
    } else {
      let behavior = "ENQUEUE";
      console.log(
        "Goes inside the enqueue with " +
        "current token: " +
        CurrentToken +
        "preivous: " +
        previousToken +
        "behavior " +
        behavior,
        offsetInMilliseconds
      );
      return handlerInput.responseBuilder
        .addAudioPlayerPlayDirective(
          behavior,
          CurrentToken,
          CurrentToken,
          offsetInMilliseconds,
          previousToken
        )
        .withStandardCard(cardTitle, cardContent, smallImageUrl, largeImageUrl)
        .getResponse();
    }
  }
}

//Now the playBacknEARLY Finsihed should BE GOOD

const PlaybackNearlyFinishedHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type ===
      "AudioPlayer.PlaybackNearlyFinished"
    );
  },

  handle(handlerInput) {
    return new Promise((resolve, reject) => {
      handlerInput.attributesManager
        .getPersistentAttributes()
        .then(attr => {
          let attributes = attr;
          // handlerInput.attributesManager.setPersistentAttributes(attributes);
          const state = attributes.state;
          let offsetInMilliseconds;
          let categorySize;
          let previousNumber;
          let previousToken;
          let nextNumber;
          let CurrentToken;
          attributes.TEST = 5;
          console.log(
            "The test in the playbacknearly Finshed is " + attributes.TEST
          );
          if (state === states.PLAY) {
            const categories = {
              history_of_space: 3,
              becoming_an_astronaut: 3,
              space_travel: 2,
              gravity_facts: 2,
              walking_in_space: 2,
              life_in_space: 6,
              cool_facts: 3
            };
            offsetInMilliseconds = attributes.offsetInMilliseconds;
            let category = attributes.category;
            let jsonCategories = JSON.stringify(categories);
            let t = JSON.parse(jsonCategories);
            let categorySize = t[category];
            previousNumber = attributes.topic;
            nextNumber = previousNumber + 1;
            previousToken = `https://s3.amazonaws.com/2018-alexa-space-stories/audio/${category}/${previousNumber}.wav`;
            CurrentToken = `https://s3.amazonaws.com/2018-alexa-space-stories/audio/${category}/${nextNumber}.wav`;
            attributes.topic = nextNumber;
          } else if (state === states.PLAYALL) {
            attributes.FirstIndex = attributes.FirstIndex + 1;
            let category = StoryName[attributes.FirstIndex];
            let CurrentNumber = StoryNumber[attributes.FirstIndex];
            //nextNumber  =  StoryNumber[  attributes.FirstIndex +1];
            console.log(
              "In the PlayBack Nearly Finsihed  Function FirstIndex: " +
              attributes.FirstIndex +
              "Category is: " +
              category +
              "Currentnumber is: " +
              CurrentNumber
            );
            let previousIndex = attributes.FirstIndex;
            previousIndex = previousIndex - 1;
            let previousForCurrent = StoryNumber[previousIndex];
            let previousCategory = StoryName[previousIndex];
            offsetInMilliseconds = attributes.offsetInMilliseconds;
            attributes.category = category;

            if (CurrentNumber == 1) {
              previousToken = null;

              if (category == "history_of_space") {
                previousToken = null;
              } else {
                previousToken = `https://s3.amazonaws.com/2018-alexa-space-stories/audio/${previousCategory}/${previousForCurrent}.wav`;
              }
            } else {
              previousNumber = CurrentNumber - 1;
              previousToken = `https://s3.amazonaws.com/2018-alexa-space-stories/audio/${category}/${previousNumber}.wav`;
            }
            CurrentToken = `https://s3.amazonaws.com/2018-alexa-space-stories/audio/${category}/${CurrentNumber}.wav`;
            //PlayBack Nearly finished update the firstIdex
            categorySize = 21;
            // console.log('In the playBackNearly Finished the previous Token is:' + previousToken +'The current token is: ' + CurrentToken + 'The first index is: ' +  attributes.FirstIndex + 'The topic is ' +  attributes.topic );
          }

          handlerInput.attributesManager
            .savePersistentAttributes()
            .then(attr => {
              resolve(
                playNearFinish(
                  handlerInput,
                  attributes,
                  categorySize,
                  nextNumber,
                  offsetInMilliseconds,
                  previousToken,
                  CurrentToken,
                  state
                )
              );
            })
            .catch(err => {
              console.log(err.message, err.stack);
              reject(err);
            });
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};

function playALL(
  handlerInput,
  attributes,
  nextNumber,
  offsetInMilliseconds,
  previousToken,
  CurrentToken,
  behavior
) {
  if (21 < nextNumber) {
    return handlerInput.responseBuilder.getResponse();
  } else {
    if (
      handlerInput.requestEnvelope.context.System.device.supportedInterfaces
      .Display
    ) {
      return handlerInput.responseBuilder
        .addAudioPlayerPlayDirective(
          behavior,
          CurrentToken,
          CurrentToken,
          offsetInMilliseconds,
          previousToken
        )
        .addRenderTemplateDirective(template)
        .getResponse();
    } else {
      return handlerInput.responseBuilder
        .addAudioPlayerPlayDirective(
          behavior,
          CurrentToken,
          CurrentToken,
          offsetInMilliseconds,
          previousToken
        )
        .withStandardCard(cardTitle, "", smallImageUrl, largeImageUrl)
        .getResponse();
    }
  }
}

//PlayAll can not save or history of space

const PlayAllIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "PlayAllIntent"
    );
  },
  handle(handlerInput) {
    let attributes = handlerInput.attributesManager.getSessionAttributes();
    attributes.state = states.PLAYALL;
    attributes.offsetInMilliseconds = 0;
    let offsetInMilliseconds = 0;
    let category = StoryName[attributes.FirstIndex];
    let CurrentNumber = StoryNumber[attributes.FirstIndex];
    console.log(
      "In the PLAYALL Function FirstIndex: " +
      attributes.FirstIndex +
      "Category is: " +
      category +
      "Currentnumber is: " +
      CurrentNumber
    );
    //First Times increument the first Index
    attributes.category = category;
    attributes.topic = CurrentNumber;
    let previous;
    let behavior;
    let previousToken;
    if (attributes.FirstIndex == 0) {
      previous = null;
      behavior = "REPLACE_ALL";
    } else {
      behavior = "ENQUEUE";
      previous = CurrentNumber - 1;
    }

    if (previous == null) {
      previousToken = null;
    } else {
      previousToken = `https://s3.amazonaws.com/2018-alexa-space-stories/audio/${category}/${previous}.wav`;
    }
    let CurrentToken = `https://s3.amazonaws.com/2018-alexa-space-stories/audio/${category}/${CurrentNumber}.wav`;
    //attributes.FirstIndex +=1;
    // console.log('Before calll playall: firstIndex: ' + attributes.FirstIndex + 'previousToken: ' + previousToken + 'CurrentToken: ' + CurrentToken);
    return playALL(
      handlerInput,
      attributes,
      attributes.FirstIndex,
      offsetInMilliseconds,
      previousToken,
      CurrentToken,
      behavior
    );
  }
};

//Need to test the next for playall
//Increument the firstIndex if the playbacknearly finisehd did not getitings stop
function next(handlerInput, attributes) {
  console.log("The test in the playbacknearly Finshed is " + attributes.TEST);
  console.log('session attr: In the next function ', handlerInput.attributesManager.getSessionAttributes());
  console.log("The attributes in the next is ", attributes);
  //So this should be the section attribute. is not the persistences.
  const state = attributes.state;
  let categorySize;
  let previousNumber;
  let nextNumber;
  let category;

  console.log("The state is " + state);

  if (state === states.PLAY) {
    console.log("goes inside the play");

    const categories = {
      history_of_space: 3,
      becoming_an_astronaut: 3,
      space_travel: 2,
      gravity_facts: 2,
      walking_in_space: 2,
      life_in_space: 6,
      cool_facts: 3
    };
    category = attributes.category;
    let jsonCategories = JSON.stringify(categories);
    let t = JSON.parse(jsonCategories);
    categorySize = t[category];
    previousNumber = attributes.topic;
    nextNumber = previousNumber + 1;

    console.log("In the next function: previousNumber is: " + previousNumber);
    // if( handlerInput.requestEnvelope.request.type === "AudioPlayer.PlaybackStarted"  ){
    //   //This might have
    //   nextNumber = previousNumber + 1;

    // }else{
    //    nextNumber = previousNumber;

    // }

    if (categorySize < nextNumber) {
      let speech =
        "There is no more topic in this category. Choose another category.";
      attributes.offsetInMilliseconds = 0;

      if (
        handlerInput.requestEnvelope.context.System.device.supportedInterfaces
        .Display
      ) {
        return handlerInput.responseBuilder
          .speak(speech)
          .addRenderTemplateDirective(template)
          .getResponse();
      } else {
        return handlerInput.responseBuilder
          .speak(speech)
          .withStandardCard(
            cardTitle,
            cardContent,
            smallImageUrl,
            largeImageUrl
          )
          .getResponse();
      }
    } else {
      let behavior = "REPLACE_ALL";
      let offsetInMilliseconds = attributes.offsetInMilliseconds;
      let url = `https://s3.amazonaws.com/2018-alexa-space-stories/audio/${category}/${nextNumber}.wav`;
      let token = url;
      attributes.topic = nextNumber;
      let speechOutput =
        "You can say, Alexa, next, to listen to the next topic.";

      if (
        handlerInput.requestEnvelope.context.System.device.supportedInterfaces
        .Display
      ) {
        return handlerInput.responseBuilder
          .speak(speechOutput)
          .addAudioPlayerPlayDirective(
            behavior,
            url,
            token,
            null,
            offsetInMilliseconds
          )
          .addRenderTemplateDirective(template)
          .getResponse();
      } else {
        return handlerInput.responseBuilder
          .addAudioPlayerPlayDirective(
            behavior,
            url,
            token,
            null,
            offsetInMilliseconds
          )
          .withStandardCard(
            cardTitle,
            cardContent,
            smallImageUrl,
            largeImageUrl
          )
          .speak(speechOutput)
          .getResponse();
      }
    }
  } else {
    // if( handlerInput.requestEnvelope.request.type === "AudioPlayer.PlaybackStarted"  ){
    category = StoryName[attributes.FirstIndex + 1];
    nextNumber = StoryNumber[attributes.FirstIndex + 1];
    console.log(
      "In the next Next Function the first index: " +
      attributes.FirstIndex +
      "category is : " +
      category +
      "nextNumber" +
      nextNumber
    );
    categorySize = 21;

    if (21 < attributes.FirstIndex) {
      let speech =
        "There is no more topic in this category. Choose another category.";
      attributes.offsetInMilliseconds = 0;

      if (
        handlerInput.requestEnvelope.context.System.device.supportedInterfaces
        .Display
      ) {
        return handlerInput.responseBuilder
          .speak(speech)
          .addRenderTemplateDirective(template)
          .getResponse();
      } else {
        return handlerInput.responseBuilder
          .speak(speech)
          .withStandardCard(
            cardTitle,
            cardContent,
            smallImageUrl,
            largeImageUrl
          )
          .getResponse();
      }
    } else {
      attributes.FirstIndex = attributes.FirstIndex + 1;
      let behavior = "REPLACE_ALL";
      let offsetInMilliseconds = attributes.offsetInMilliseconds;
      let url = `https://s3.amazonaws.com/2018-alexa-space-stories/audio/${category}/${nextNumber}.wav`;
      let token = url;
      let speechOutput =
        "You can say, Alexa, next, to listen to the next topic.";

      if (
        handlerInput.requestEnvelope.context.System.device.supportedInterfaces
        .Display
      ) {
        return handlerInput.responseBuilder
          .speak(speechOutput)
          .addAudioPlayerPlayDirective(
            behavior,
            url,
            token,
            null,
            offsetInMilliseconds
          )
          .addRenderTemplateDirective(template)
          .getResponse();
      } else {
        return handlerInput.responseBuilder
          .addAudioPlayerPlayDirective(
            behavior,
            url,
            token,
            null,
            offsetInMilliseconds
          )
          .withStandardCard(
            cardTitle,
            cardContent,
            smallImageUrl,
            largeImageUrl
          )
          .speak(speechOutput)
          .getResponse();
      }
    }
  }
}
//PlayBackNearly Finisehd get load. vairable increment 
const NextIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "AMAZON.NextIntent"
    );
  },
  handle(handlerInput) {
    return new Promise((resolve, reject) => {
      handlerInput.attributesManager
        .getPersistentAttributes()

        .then(attributes => {
          handlerInput.attributesManager.setSessionAttributes(attributes);
          attributes.offsetInMilliseconds = 0;
          console.log('In the next function the attribtues is ', attributes);
          resolve(next(handlerInput, attributes));
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};

const HistoryOfSpaceIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name ===
      "historyOfSpaceIntent"
    );
  },
  handle(handlerInput) {
    let attributes = handlerInput.attributesManager.getSessionAttributes();
    attributes.category = "history_of_space";
    attributes.offsetInMilliseconds = 0;
    attributes.topic = 1;
    attributes.state = states.PLAY;
    return play(handlerInput, attributes);
  }
};

const BecomingAnAstronauntIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name ===
      "becomingAnAstronautIntent"
    );
  },
  handle(handlerInput) {
    let attributes = handlerInput.attributesManager.getSessionAttributes();
    attributes.category = "becoming_an_astronaut";
    attributes.offsetInMilliseconds = 0;
    attributes.topic = 1;
    attributes.state = states.PLAY;
    return play(handlerInput, attributes);
  }
};

const SpaceTravelIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "spaceTravelIntent"
    );
  },
  handle(handlerInput) {
    let attributes = handlerInput.attributesManager.getSessionAttributes();
    attributes.category = "space_travel";
    attributes.offsetInMilliseconds = 0;
    attributes.topic = 1;
    attributes.state = states.PLAY;
    return play(handlerInput, attributes);
  }
};

const GravityFactsIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "gravityFactsIntent"
    );
  },
  handle(handlerInput) {
    let attributes = handlerInput.attributesManager.getSessionAttributes();
    attributes.category = "gravity_facts";
    attributes.offsetInMilliseconds = 0;
    attributes.topic = 1;
    attributes.state = states.PLAY;
    return play(handlerInput, attributes);
  }
};

const WalkingInSpaceIntentIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name ===
      "walkingInSpaceIntent"
    );
  },
  handle(handlerInput) {
    let attributes = handlerInput.attributesManager.getSessionAttributes();
    attributes.category = "walking_in_space";
    attributes.offsetInMilliseconds = 0;
    attributes.topic = 1;
    attributes.state = states.PLAY;
    return play(handlerInput, attributes);
  }
};

const LifeInSpaceIntentIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "lifeInSpaceIntent"
    );
  },
  handle(handlerInput) {
    let attributes = handlerInput.attributesManager.getSessionAttributes();
    attributes.category = "life_in_space";
    attributes.offsetInMilliseconds = 0;
    attributes.topic = 1;
    attributes.state = states.PLAY;
    return play(handlerInput, attributes);
  }
};

const CoolFactsIntentIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "coolFactsIntent"
    );
  },
  handle(handlerInput) {
    let attributes = handlerInput.attributesManager.getSessionAttributes();
    attributes.category = "life_in_space";
    attributes.offsetInMilliseconds = 0;
    attributes.topic = 1;
    attributes.state = states.PLAY;
    return play(handlerInput, attributes);
  }
};

const LearnmoreIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "learnMoreIntent"
    );
  },
  handle(handlerInput) {
    const speech =
      "To learn more about Mike and his space stories go to, www dot mike massimino dot com ";

    if (
      handlerInput.requestEnvelope.context.System.device.supportedInterfaces
      .Display
    ) {
      return handlerInput.responseBuilder
        .speak(speech)
        .reprompt(speech)
        .addRenderTemplateDirective(template)
        .getResponse();
    } else {
      return handlerInput.responseBuilder
        .speak(speech)
        .reprompt(speech)
        .withStandardCard(cardTitle, "", smallImageUrl, largeImageUrl)
        .getResponse();
    }
  }
};

const StartOverIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name ===
      "AMAZON.StartOverIntent"
    );
  },
  handle(handlerInput) {
    return new Promise((resolve, reject) => {
      handlerInput.attributesManager
        .getPersistentAttributes()

        .then(attributes => {
          handlerInput.attributesManager.setSessionAttributes(attributes);
          attributes.offsetInMilliseconds = 0;
          resolve(play(handlerInput, attributes));
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};
const ResumeIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "AMAZON.ResumeIntent"
    );
  },
  handle(handlerInput) {
    return new Promise((resolve, reject) => {
      handlerInput.attributesManager
        .getPersistentAttributes()
        .then(attr => {
          //let attributes = attr;
          console.log('session attr: ', handlerInput.attributesManager.getSessionAttributes());
          console.log("The resume intent get in", attr);
          console.log("The resume intent get in" + attr.offsetInMilliseconds);
          resolve(play(handlerInput, attr));
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};

function stop(handlerInput, attributes) {
  if (
    handlerInput.requestEnvelope.context.System.device.supportedInterfaces
    .Display
  ) {
    return handlerInput.responseBuilder
      .addAudioPlayerStopDirective()
      .reprompt("")
      .addRenderTemplateDirective(template)
      .getResponse();
  } else {
    return handlerInput.responseBuilder
      .addAudioPlayerStopDirective()
      .reprompt("")
      .withStandardCard(cardTitle, cardContent, smallImageUrl, largeImageUrl)
      .getResponse();
  }
}

const PauseIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "AMAZON.PauseIntent"
    );
  },
  handle(handlerInput) {
    console.log('pause intent session attr: ', handlerInput.attributesManager.getSessionAttributes());
    return new Promise((resolve, reject) => {
      handlerInput.attributesManager.getPersistentAttributes().then(attr => {
        let attributes = attr;
        console.log('The attributes in the pause is equal to for attribute', attributes);
        console.log('The attributes in the pause is equal to for attr', attr);

        attributes.offsetInMilliseconds =
          handlerInput.requestEnvelope.context.AudioPlayer.offsetInMilliseconds;
        //Here need to save the category and the 
        //For beginner we need to save the category and the names; 



        handlerInput.attributesManager.setPersistentAttributes(attributes);
        handlerInput.attributesManager.setSessionAttributes(attributes);
        handlerInput.attributesManager
          .savePersistentAttributes()

          .then(attr => {
            console.log(
              "The offset In the pause intent is " +
              attributes.offsetInMilliseconds
            );
            handlerInput.attributesManager.getPersistentAttributes().then(attrs => {
              console.log('attrs: ', attrs);
              resolve(stop(handlerInput, attributes));
            })
          })
          .catch(err => {
            console.log(err.message, err.stack);
            reject(err);
          });
      });
    }).catch(error => {
      reject(error);
    });
  }
};

const StopIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "AMAZON.StopIntent"
    );
  },
  handle(handlerInput) {


    return handlerInput.responseBuilder
      .addAudioPlayerStopDirective()
      .speak("Okay")
      .getResponse();
  }
};

const CancelAndStopIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      (handlerInput.requestEnvelope.request.intent.name ===
        "AMAZON.CancelIntent" ||
        handlerInput.requestEnvelope.request.intent.name ===
        "AMAZON.StopIntent")
    );
  },
  handle(handlerInput) {
    const speechText = "Goodbye!";
    return handlerInput.responseBuilder.speak(speechText).getResponse();
  }
};

const HelpIntentHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "AMAZON.HelpIntent"
    );
  },
  handle(handlerInput) {
    const speech =
      "You can say space story to choose a category of space stories." +
      " Say biography to listen to Mike's bio. " +
      "Or say learn more to get information about his website.";

    if (
      handlerInput.requestEnvelope.context.System.device.supportedInterfaces
      .Display
    ) {
      return handlerInput.responseBuilder
        .speak(speech)
        .reprompt(speech)
        .addRenderTemplateDirective(template)
        .getResponse();
    } else {
      return handlerInput.responseBuilder
        .speak(speech)
        .reprompt(speech)
        .withStandardCard(cardTitle, "", smallImageUrl, largeImageUrl)
        .getResponse();
    }
  }
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === "SessionEndedRequest";
  },
  handle(handlerInput) {
    return handlerInput.responseBuilder.getResponse();
  }
};

const UnhandledHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "IntentRequest" &&
      handlerInput.requestEnvelope.request.intent.name === "Unhandled"
    );
  },
  handle(handlerInput) {
    const speech =
      "I didn't quite catch that. Say biography or space story, or help. ";
    return handlerInput.responseBuilder.speak(speech);
  }
};

// const PersistenceSavingResponseInterceptor = {
//   process(handlerInput) {
//     return new Promise((resolve, reject) => {
//       //Pause and resume does not work cause the follwing is getting cause, and therefore not save the data
//       if (
//         handlerInput.requestEnvelope.request.type !==
//           "AudioPlayer.PlaybackNearlyFinished" &&
//         handlerInput.requestEnvelope.request.type !==
//           "AudioPlayer.PlaybackFinished" &&
//         handlerInput.requestEnvelope.request.type !==
//           "AudioPlayer.PlaybackStarted" &&
//         handlerInput.requestEnvelope.request.type !==
//           "AudioPlayer.PlaybackStopped" &&
//         handlerInput.requestEnvelope.request.type !==
//           "AudioPlayer.PlaybackFailed"
//       ) {
//         let sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
//         handlerInput.attributesManager.setPersistentAttributes(
//           sessionAttributes
//         );
//       }

//       handlerInput.attributesManager
//         .savePersistentAttributes()
//         .then(() => {
//           resolve();
//         })
//         .catch(error => {
//           reject(error);
//         });
//     });
//   }
// };


const PersistenceSavingResponseInterceptor = {
  process(handlerInput) {
    return new Promise((resolve, reject) => {


      //Pause and resume does not work cause the follwing is getting cause, and therefore not save the data

      if ((handlerInput.requestEnvelope.request.type !== "AudioPlayer.PlaybackNearlyFinished" &&
          handlerInput.requestEnvelope.request.type !== "AudioPlayer.PlaybackFinished" &&
          handlerInput.requestEnvelope.request.type !== "AudioPlayer.PlaybackStarted" &&
          handlerInput.requestEnvelope.request.type !== "AudioPlayer.PlaybackStopped" &&
          handlerInput.requestEnvelope.request.type !== "AudioPlayer.PlaybackFailed")) {

        if (handlerInput.requestEnvelope.request.intent) {
          if (handlerInput.requestEnvelope.request.intent.name !== "AMAZON.NextIntent") {
            let sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
            handlerInput.attributesManager.setPersistentAttributes(sessionAttributes);

          }

        }

      }

      handlerInput.attributesManager.savePersistentAttributes()



        .then(() => {
          resolve();
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};

//Default audio

const PlaybackStartedHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type ===
      "AudioPlayer.PlaybackStarted"
    );
  },

  handle(handlerInput) {
    if (
      handlerInput.requestEnvelope.context.System.device.supportedInterfaces
      .Display
    ) {
      return handlerInput.responseBuilder
        .addRenderTemplateDirective(template)
        .getResponse();
    } else {
      return handlerInput.responseBuilder
        .withStandardCard(cardTitle, cardContent, smallImageUrl, largeImageUrl)
        .getResponse();
    }
  }
};
const PlaybackFinishedHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type ===
      "AudioPlayer.PlaybackFinished"
    );
  },

  handle(handlerInput) {
    return new Promise((resolve, reject) => {
      handlerInput.attributesManager
        .getPersistentAttributes()
        .then(attributes => {
          handlerInput.attributesManager.setPersistentAttributes(attributes);
          return handlerInput.attributesManager.savePersistentAttributes();
        })
        .then(() => {
          resolve(handlerInput.responseBuilder.getResponse());
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};

const PlaybackStoppedHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type ===
      "AudioPlayer.PlaybackStopped"
    );
  },

  handle(handlerInput) {}
};

const PlaybackFailedHandler = {
  canHandle(handlerInput) {
    return (
      handlerInput.requestEnvelope.request.type === "AudioPlayer.PlaybackFailed"
    );
  },

  handle(handlerInput) {}
};

function setState(handlerInput, state) {
  let attributes = handlerInput.attributesManager.getSessionAttributes();
  attributes.state = state;
}

const skillBuilder = Alexa.SkillBuilders.standard();
exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    AuthorBioIntentHandler,
    SpaceStoryIntentHandler,
    ListStoryIntentHandler,
    HistoryOfSpaceIntentHandler,
    PlaybackNearlyFinishedHandler,
    BecomingAnAstronauntIntentHandler,
    SpaceTravelIntentHandler,
    GravityFactsIntentHandler,
    WalkingInSpaceIntentIntentHandler,
    LifeInSpaceIntentIntentHandler,
    CoolFactsIntentIntentHandler,
    LearnmoreIntentHandler,
    PlayAllIntentHandler,
    NextIntentHandler,
    ResumeIntentHandler,
    StartOverIntentHandler,
    PauseIntentHandler,
    StopIntentHandler,
    HelpIntentHandler,
    CancelAndStopIntentHandler,
    UnhandledHandler,
    PlaybackStartedHandler,
    PlaybackFinishedHandler,
    PlaybackStoppedHandler,
    PlaybackFailedHandler,
    SessionEndedRequestHandler
  )
  .addErrorHandlers(ErrorHandler)
  .withTableName("2018_Alexa_Space_Stories_New")
  .withAutoCreateTable(true)
  .addResponseInterceptors(PersistenceSavingResponseInterceptor)
  .lambda();

//Below is the code for the audio play function
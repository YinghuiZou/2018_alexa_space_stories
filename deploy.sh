#!/usr/bin/env bash

echo 'installing package...'
npm install > /dev/null
echo 'install complete...'

echo 'creating build...'
mkdir submission
zip -X -r submission/index.zip * -x build build/* *.xlsx Skills Skills/* test test/* speechAssets speechAssets/* index.zip > /dev/null
echo 'build complete...'

cd submission

# Set up environment variable. Remember to change them when
# switch to stage or live
BRANCH_NAME=`git name-rev --name-only HEAD`
echo ${BRANCH_NAME}

if [ "${BRANCH_NAME}" == "master" ]; then
  FNAME=2018_Alexa_Space_Stories
  APP_ID=amzn1.ask.skill.f3c620c6-b2f6-4ba4-a8e4-c0f712ef64d0
  ALIAS=LIVE
  declare -a OTHER_ALIASES=(staging LIVE-1)
else
  FNAME=2018_Alexa_Space_Stories
  APP_ID=amzn1.ask.skill.f3c620c6-b2f6-4ba4-a8e4-c0f712ef64d0
  ALIAS=staging
  declare -a OTHER_ALIASES=(LIVE LIVE-1)
fi

# GET Total number of functions
echo 'getting number of versions...'
FUNCTION_AMT=`aws lambda list-versions-by-function --function-name $FNAME | jq '.Versions|length'`
echo $FUNCTION_AMT

# Update function or create a new one if not exits
echo 'updating lambda...'
aws lambda update-function-code --function-name $FNAME --zip-file fileb://index.zip | grep Version
aws lambda update-function-configuration --function-name $FNAME --environment "Variables={APP_ID=$APP_ID}"

# Publish a new version and get the new version number
version=`aws lambda publish-version --function-name $FNAME | grep Version | awk '{gsub(/[",]/, "", $2);print $2}'`
echo $version

# Point the alias to the newly updated one
echo 'updating alias to the new version...'
UPDATE=`aws lambda update-alias --function-name $FNAME --name $ALIAS --function-version $version`
echo $UPDATE

echo 'deleting old versions...'
if [ $FUNCTION_AMT -gt 5 ]
then
  START_VERSION=`aws lambda list-versions-by-function --function-name $FNAME | jq '.Versions[1].Version' | awk '{gsub(/[",]/, "", $1);print $1}'`
  echo 'Start version ='
  echo $START_VERSION
  echo 'Function AMT ='
  echo $FUNCTION_AMT
  for ((i=$START_VERSION; i < $START_VERSION + $FUNCTION_AMT - 5; ++i))
  do

    #if [ "${BRANCH_NAME}" == "staging" ]; then
#if ["$i"!="LIVE"]; then
    aws lambda delete-function --function-name $FNAME --qualifier $i
    

   # fi
   # fi




  done

fi

cd ..